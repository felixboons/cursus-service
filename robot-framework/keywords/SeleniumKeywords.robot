*** Settings ***
Library      SeleniumLibrary

*** Variable ***
${DOCKER}    False

*** Keywords ***
Setup Browser
    [Arguments]                         ${url}
    Run Keyword If                      '${DOCKER}' == 'True'                                        Open Browser In Jenkins          ${url}
    Run Keyword If                      '${DOCKER}' != 'True'                                        Open Browser On Local Machine    ${url}

Open Browser On Local Machine
    [Arguments]                         ${URL}
    Log                                 Running RobotFramework Local
    Set Selenium Speed                  ${SELENIUM_DELAY}
    Open Browser                        ${URL}                                                       browser=chrome

Open Browser In Jenkins
    [Arguments]                         ${URL}
    Log                                 Running RobotFramework Jenkins
    Set Selenium Speed                  ${SELENIUM_DELAY}
    Import Library                      XvfbRobot
    Start Virtual Display               1920                                                         1080
    Open Browser                        ${URL}                                                       browser=firefox
    Set Window Size                     1920                                                         1080

Select Cursus From Cursuslist
    [Arguments]                         ${TRAINING_NAME}
    Wait Until Page Contains Element    id=create                                                    timeout=1                        error=Kan geen training selecteren, lijst is niet beschikbaar
    Click Element                       //*[contains(text(),'${TRAINING_NAME}')]

Select Login Button
    Wait Until Page Contains Element    id=login                                                    timeout=1                        error=Kan geen training selecteren, lijst is niet beschikbaar
    Click Element                       //*[@id="login"]

Click Cursus Details Button
    Wait Until Page Contains Element    //*[@class="expanded"]//*[@id="goToTrainingDetails"]         timeout=1
    Click Element                       //*[@class="expanded"]//*[@id="goToTrainingDetails"]

Click Cursus Wijzigen Button
    Wait Until Page Contains Element    //*[@class="expanded"]//*[@id="editTraining"]                timeout=1
    Click Element                       //*[@class="expanded"]//*[@id="editTraining"]

Click Cursus Verwijderen Button
    Wait Until Page Contains Element    //*[@class="expanded"]//*[contains(text(),'Verwijderen')]    timeout=1
    Click Element                       //*[@class="expanded"]//*[contains(text(),'Verwijderen')]
