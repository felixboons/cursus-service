*** Settings ***
Resource                             ../keywords/SeleniumKeywords.robot
Suite Teardown                       Close All Browsers
Documentation                        Keyword documentation: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
...
...                                  Kijk goed of er keywords zijn die je kan gebruiken in de Keywords/SeleniumKeywords.robot file

*** Variable ***
${SELENIUM_DELAY}                    .2
${TEST_URL}                          http://cursusclient-${GROUP_NAME}-hco-kza-cursus.apps.prod.am5.hotcontainers.nl/cursussen
${GROUP_NAME}                        gryffindor

*** Test Cases ***
#Opdracht 0: Google Test
## Als deze test werkt dan werkt RobotFramework goed. Verwijder deze als je begint.}
#Setup Browser                       https://www.google.com
#Set Selenium Speed                  ${SELENIUM_DELAY}
#Wait Until Page Contains Element    name=q                                                                                           timeout=5

Opdracht 1: Login in de cursusclient
    Setup Browser                       ${TEST_URL}
    Set Selenium Speed                  ${SELENIUM_DELAY}
    Select Login Button

Opdracht 2: Controleer of de pagina correct geladen is
    Wait Until Page Contains Element    //*[contains(text(),'Ingelogd als Avans student')]                             timeout=1

Opdracht 3: Open het scherm om een nieuwe training aan te maken
    Click Element                       id=create
    Wait Until Page Contains Element    //*[contains(text(),'Cursus aanmaken')]                                        timeout=1

Opdracht 4: Maak een nieuwe training aan
    Input Text                          id=trainingName                                                                Testtraining
    Select From List By Index           id=selectAttitude                                                              0
    Select From List By Index           id=functieniveau                                                               0
    Select From List By Index           id=passingCriteria                                                             0
    Select From List By Index           id=status                                                                      0
    Input Text                          id=maxParticipants                                                             1
    Input Text                          id=trainerName                                                                 John Doe
    Input Text                          id=trainerEmail                                                                jd@mail.com
    Click Element                       id=setDefaultDate
    Input Text                          id=trainingDescription                                                         Dit is een beschrijving
    Wait Until Element Is Enabled       id=save                                                                        timeout=3                  error=Knop is niet enabled
    Click Element                       id=save
    Wait Until Page Contains Element    //*[contains(text(),'Testtraining')]                                           timeout=5

Opdracht 5: Meld je aan voor de training die je net hebt gemaakt
    Wait Until Page Contains Element    id=aanmelden                                                                   timeout=5
    Click Element                       id=aanmelden
    Wait Until Page Contains Element    id=ja                                                                          timeout=5
    Click Element                       id=ja
    Sleep                               1
    Click Element                       id=terug
    Wait Until Page Contains Element    //*[contains(text(),'1/1')]                                                    timeout=5

Opdracht 6: Controleer of de aangemaakte training in de lijst van trainingen terugkomt
    Page Should Contain                 Testtraining

Opdracht 7: Controleer de details pagina van de aangemaakte training
    Select Cursus From Cursuslist       Testtraining
    Click Cursus Details Button
    Wait Until Page Contains Element    //*[contains(text(),'Testtraining')]                                           timeout=1
    Wait Until Page Contains Element    //*[contains(text(),'Test attitude')]                                          timeout=1
    Wait Until Page Contains Element    //*[contains(text(),'Dit is een beschrijving')]                                timeout=1
    Click Element                       id=terug
    Wait Until Page Contains Element    //*[contains(text(),'Cursus kalender')]                                        timeout=1

Opdracht 8: Verwijder de aangemaakte training
    Select Cursus From Cursuslist       Testtraining
    Click Cursus Verwijderen Button
    Wait Until Page Contains Element    id=yesImSure                                                                   timeout=5
    Click Element                       id=yesImSure
    Wait Until Page Contains Element    //*[contains(text(),'Ingelogd als Avans student')]                             timeout=1
    Page Should Not Contain             Testtraining

Opdracht 9: Test de logout functionaliteit
    Click Element                       id=logout
    Wait Until Page Contains Element    //*[contains(text(),'Log in voor een actueel overzicht van de cursussen.')]    timeout=1
