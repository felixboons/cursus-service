package nl.kzaconnected.cursus.service;

import nl.kzaconnected.cursus.configuration.ResourceNotFoundException;
import nl.kzaconnected.cursus.model.Dao.*;
import nl.kzaconnected.cursus.model.Dto.CursusDto;
import nl.kzaconnected.cursus.repository.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class CursusServiceTest {

    @TestConfiguration
    static class CursusServiceTestContextConfiguration {

        @Bean
        public CursusService cursusService() {
            return new CursusService();
        }
    }

    @Autowired
    private CursusService cursusService;
    @MockBean
    private CursusRepository cursusRepository;
    @MockBean
    private AttitudeRepository attitudeRepository;
    @MockBean
    private SlagingscriteriumRepository slagingscriteriumRepository;
    @MockBean
    private FunctieniveauRepository functieniveauRepository;
    @MockBean
    private StatusRepository statusRepository;

    private List<CursusDto> cursusDtos;
    private List<Cursus> cursussen;
    private Attitude attitude;
    private Slagingscriterium slagingscriterium;
    private Functieniveau functieniveau;
    private Status status;

    @Before
    public void init() throws Exception {
        attitude = Attitude.builder().id(1L).attitude("Test attitude").build();
        slagingscriterium = Slagingscriterium.builder().id(1L).slagingscriterium("Cursusavonden gevolgd").build();
        functieniveau = Functieniveau.builder().id(1L).functieniveau("Trainee").build();
        status = Status.builder().id(1L).status("Nog in te plannen").build();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        List<Datum> cursusData = new ArrayList<>();
        cursusData.add(Datum.builder().datum(simpleDateFormat.parse("01-01-2018 08:00:00")).build());
        cursusData.add(Datum.builder().datum(simpleDateFormat.parse("01-01-2019 09:00:00")).build());

        List<Docent> cursusDocenten = new ArrayList<>();
        cursusDocenten.add(Docent.builder().naam("Docent1").email("test@test.nl").build());
        cursusDocenten.add(Docent.builder().naam("Docent2").email("test@test.nl").build());

        CursusDto cursusDto1 = CursusDto.builder()
                .id(1L)
                .naam("Cursus1")
                .attitude(attitude.getAttitude())
                .slagingscriterium(slagingscriterium.getSlagingscriterium())
                .functieniveau(functieniveau.getFunctieniveau())
                .status(status.getStatus())
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .maxdeelnemers(1)
                .beschrijving("Beschrijving1")
                .build();

        CursusDto cursusDto2 = CursusDto.builder()
                .id(2L)
                .naam("Cursus2")
                .attitude(attitude.getAttitude())
                .slagingscriterium(slagingscriterium.getSlagingscriterium())
                .functieniveau(functieniveau.getFunctieniveau())
                .status(status.getStatus())
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .maxdeelnemers(2)
                .beschrijving("Beschrijving2")
                .build();

        cursusDtos = new ArrayList<>();
        cursusDtos.add(cursusDto1);
        cursusDtos.add(cursusDto2);

        Cursus cursus1 = Cursus.builder()
                .id(1L)
                .naam("Cursus1")
                .attitude(attitude)
                .slagingscriterium(slagingscriterium)
                .functieniveau(functieniveau)
                .status(status)
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .maxDeelnemers(1)
                .beschrijving("Beschrijving1")
                .build();

        Cursus cursus2 = Cursus.builder()
                .id(2L)
                .naam("Cursus2")
                .attitude(attitude)
                .slagingscriterium(slagingscriterium)
                .functieniveau(functieniveau)
                .status(status)
                .cursusdata(cursusData)
                .cursusdocenten(cursusDocenten)
                .maxDeelnemers(2)
                .beschrijving("Beschrijving2")
                .build();

        cursussen = new ArrayList<>();
        cursussen.add(cursus1);
        cursussen.add(cursus2);
    }

    @Test
    public void findOneShouldReturnCursusDto() {
        Mockito.when(cursusRepository.findById(1L))
                .thenReturn(Optional.of(cursussen.get(0)));
        CursusDto found = cursusService.findOne(1L);
        Assert.assertEquals(found, cursusDtos.get(0));
    }

    @Test
    public void findAllShouldReturnListOfCursusDtos() {
        Mockito.when(cursusRepository.findAll())
                .thenReturn(cursussen);
        List<CursusDto> found = cursusService.findAll();
        for (CursusDto cursusDto : found) {
            Assert.assertTrue(cursusDtos.stream().anyMatch(cursusDto::equals));
        }
    }

    @Test
    public void saveShouldReturnCursusDtoIfValid() {
        Mockito.when(cursusRepository.save(cursussen.get(0)))
                .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
                .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
                .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
                .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
                .thenReturn(status);
        CursusDto found = cursusService.save(cursusDtos.get(0));
        Assert.assertEquals(found, cursusDtos.get(0));
    }

    @Test
    public void deleteShouldCallRepositoryMethodDelete() {
        Mockito.when(cursusRepository.findById(1L))
                .thenReturn(Optional.of(cursussen.get(0)));
        cursusService.delete(1L);
        verify(cursusRepository).delete(cursussen.get(0));
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void saveShouldThrowExceptionIfCursusNameTooLong() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Cursus naam is te lang");

        cursusDtos.get(0).setNaam("Dit is een te lange naam en zou dus moeten falen met een exception");
        Mockito.when(cursusRepository.save(cursussen.get(0)))
                .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
                .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
                .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
                .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
                .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void findOneShouldNotReturnCursusDto() {
        thrown.expect(ResourceNotFoundException.class);

        CursusDto result = cursusService.findOne(5L);
        Assert.assertNull(result);
    }

    @Test
    public void findAllShouldReturnEmptyListOfCursusDtos() {
//        List<CursusDto> emptyCursussen = new ArrayList<>();
//        Mockito.when(cursusRepository.findAll())
//            .thenReturn(emptyCursussen);
//        List<CursusDto> found = cursusService.findAll();
//        Assert.assertEquals(found.size(), emptyCursussen.size());
    }

    @Test
    public void saveShouldThrowExceptionIfAttitudeInvalid() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Attitude bestaat niet");

        cursusDtos.get(0).setAttitude("invalid attitude");
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void saveShouldThrowExceptionIfSlagingscriteriumInvalid() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Slagingscriterium bestaat niet");

        cursusDtos.get(0).setSlagingscriterium("invalid slagingscriterium");
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void saveShouldThrowExceptionIfStatusInvalid() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Status bestaat niet");

        cursusDtos.get(0).setStatus("invalid status");
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void saveShouldThrowExceptionIfFunctieniveauInvalid() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Functieniveau bestaat niet");

        cursusDtos.get(0).setFunctieniveau("invalid functieniveau");
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void saveShouldThrowExceptionIfDocentenEmpty() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Docenten moet gevuld zijn");

        cursusDtos.get(0).setCursusdocenten(new ArrayList<Docent>());
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void saveShouldThrowExceptionIfDocentNameTooLong() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Docenten naam mag niet langer zijn dan 32 karakters");

        cursusDtos.get(0).getCursusdocenten().get(0).setNaam("Dit is een hele lange naam, langer dan 32 characters");
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void saveShouldThrowExceptionIfDocentEmailInvalid() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Docenten email moet valide zijn");

        cursusDtos.get(0).getCursusdocenten().get(0).setEmail("notvalid@email");
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void saveShouldThrowExceptionIfCursistNameTooLong() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Cursist naam mag niet langer zijn dan 32 karakters");

        Cursist cursist = Cursist.builder()
            .id(1L)
            .naam("Dit is een hele lange naam, langer dan 32 characters")
            .email("cursist@email")
            .build();
        List<Cursist> cursisten = new ArrayList<>();
        cursisten.add(cursist);

        cursusDtos.get(0).setCursuscursisten(cursisten);
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void saveShouldThrowExceptionIfCursistEmailInvalid() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Cursist email moet valide zijn");

        Cursist cursist = Cursist.builder()
            .id(1L)
            .naam("Cursistje")
            .email("cursist@email")
            .build();
        List<Cursist> cursisten = new ArrayList<>();
        cursisten.add(cursist);

        cursusDtos.get(0).setCursuscursisten(cursisten);
        Mockito.when(cursusRepository.save(cursussen.get(0)))
            .thenReturn(cursussen.get(0));
        Mockito.when(attitudeRepository.findByAttitude(attitude.getAttitude()))
            .thenReturn(attitude);
        Mockito.when(slagingscriteriumRepository.findBySlagingscriterium(slagingscriterium.getSlagingscriterium()))
            .thenReturn(slagingscriterium);
        Mockito.when(functieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau()))
            .thenReturn(functieniveau);
        Mockito.when(statusRepository.findByStatus(status.getStatus()))
            .thenReturn(status);
        cursusService.save(cursusDtos.get(0));
    }

    @Test
    public void deleteShouldThrowExceptionIfCursusDtoNotFound() {
        thrown.expect(ResourceNotFoundException.class);
        cursusService.delete(3L);
    }
}
